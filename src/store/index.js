import Vue from "vue";
import Vuex from "vuex";
import products from "../data/products"
Vue.use(Vuex);
let cart = window.localStorage.getItem('cart')
let count = window.localStorage.getItem('count')
export default new Vuex.Store({
  state: {
    products,
    count: count ? JSON.parse(count) : 0,
    cart: cart ? JSON.parse(cart) : [],
  },
  actions: {

    addProduct: ({
      commit
    }, items) => {
      commit('appendProduct', items)
    },
    incrementCount: ({
      commit
    }) => {
      commit('max', )
    },
    decrementCount: ({
      commit
    }) => {
      commit('min', )
    },


  },

  mutations: {
    appendProduct: (state, items) => {
      state.cart.push(items)
      window.localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    max: (state) => {
      state.count = (state.count++) + 1
      const count = JSON.stringify(state.count);
      localStorage.setItem('count', count);
    },
    min: (state) => {
      state.count = state.count - 1
      const count = JSON.stringify(state.count);
      localStorage.setItem('count', count);
    },


  },

  modules: {},

});