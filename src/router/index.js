import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Bag from "../views/Bag.vue"
import Login from "../views/login.vue"

Vue.use(VueRouter);

const routes = [{
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/bag",
    name: "bag",
    component: Bag,
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },


];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;