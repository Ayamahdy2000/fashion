export default [{

        id: 0,
        image: require('@/images/dress1.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0

    },
    {
        id: 1,
        image: require('@/images/dress2.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0

    },
    {
        id: 2,
        image: require('@/images/dress3.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0

    },
    {
        id: 3,
        image: require('@/images/dress4.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0

    },
    {
        id: 4,
        image: require('@/images/dress5.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0

    },
    {
        id: 5,
        image: require('@/images/dress6.jpg'),
        name: "Moon City - Floral Print Spaghetti-Strap Dress",
        price: "US$ 11.59",
        quantity: 0
    }
]